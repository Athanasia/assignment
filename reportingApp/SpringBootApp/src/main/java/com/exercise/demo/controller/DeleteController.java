package com.exercise.demo.controller;

import com.exercise.demo.service.DeleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class DeleteController {
    @Autowired
    private DeleteService deleteService;

    @DeleteMapping(value = "delete/report/{reportIds}")
    public ResponseEntity<?> deleteReportByReportId(@PathVariable Optional<String> reportIds){
        String result = deleteService.deleteReports(reportIds.orElse(" "));
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }


}
