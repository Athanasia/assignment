package com.exercise.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import lombok.*;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Report {

    @Id
    @GeneratedValue
    private int reportId;
    @Column(nullable = false)
    private String title,description,priority;

    @ManyToOne(optional=false)
    @JoinColumn(name="employee",referencedColumnName="employeeId")
    private Employee employee;

    @Override
    public String toString() {
        return "Report{" +
                "reportId=" + reportId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priority='" + priority + '\'' +
                ", employee=" + employee +
                '}';
    }
}


