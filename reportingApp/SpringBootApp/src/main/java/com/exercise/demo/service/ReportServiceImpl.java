package com.exercise.demo.service;

import com.exercise.demo.model.PriorityTypes;
import com.exercise.demo.model.Report;
import com.exercise.demo.model.Employee;
import com.exercise.demo.repository.ReportRepository;
import com.exercise.demo.repository.EmployeeRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    public static final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);

    @Override
    public Page<Report> getAllReports(Pageable pageable) {
        return reportRepository.findAllReports(pageable);
    }

    @Override
    public void parseJsonFile(String filename) {
        List<Report> reportList = new ArrayList<>();
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(filename);
            JSONTokener tokener = new JSONTokener(stream);
            JSONArray reportArray = new JSONArray(tokener);

            for (int i = 0; i < reportArray.length(); i++){
                Report newReport = new Report();
                JSONObject reportObject = reportArray.getJSONObject(i);
                newReport = reportInit(reportObject);
                reportRepository.save(newReport);
                reportList.add(newReport);
                for(Report r:reportList)
                    log.debug(r.toString());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean checkPriority(String priority) {
        if(StringUtils.isEmpty(priority))
            return false;
        boolean isPriorityCorrect=false;
        for(PriorityTypes type: PriorityTypes.values()){
            if(type.name().equalsIgnoreCase(priority)){
                isPriorityCorrect=true;
            }
        }
        return isPriorityCorrect;
    }

    private Report reportInit(JSONObject reportObject) {
        Report newReport = new Report();
        String title = reportObject.getString("title");
        newReport.setTitle(title);
        String description = reportObject.getString("description");
        newReport.setDescription(description);
        String priority = reportObject.getString("priority");
        newReport.setPriority(priority);
        newReport.setEmployee(employeeInit(reportObject.getJSONArray("employee").getJSONObject(0),newReport));
        return newReport;
    }

    public Employee employeeInit(JSONObject employee, Report report) {
        Optional optionalEmployee = employeeRepository.findAll().stream()
                .filter(writer1 -> writer1.getUsername().equalsIgnoreCase(employee.getString("username")))
                .findFirst();
        if(optionalEmployee.isPresent()){
            Employee employeee = (Employee) optionalEmployee.get();
            employeee.getReports().add(report);
            return employeee;
        }

        Employee newEmployee = new Employee();
        newEmployee.setFirstName(employee.getString("firstName"));
        newEmployee.setLastName(employee.getString("lastName"));
        newEmployee.setEmail(employee.getString("email"));
        newEmployee.setGender(employee.getString("gender"));
        newEmployee.setTitle(employee.getString("title"));
        newEmployee.setUsername(employee.getString("username"));
        employeeRepository.save(newEmployee);

        return newEmployee;
    }


}
