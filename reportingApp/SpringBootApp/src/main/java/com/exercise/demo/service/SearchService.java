package com.exercise.demo.service;

import com.exercise.demo.model.Report;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface SearchService {
    Page<Report> searchByUsernameAndPriority(String username, String priority, Pageable pageable);

    Page<Report> getByPriority(String priority, Pageable pageable);

    Page<Report> getByUsername(String username, Pageable pageable);
}
