package com.exercise.demo.service;

import com.exercise.demo.model.Report;
import com.exercise.demo.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class SearchServiceImpl implements SearchService{
    @Autowired
    private ReportRepository reportRepository;

    @Override
    public Page<Report> getByPriority(String priority, Pageable pageable) {
        return reportRepository.findByPriority( priority, pageable);
    }

    @Override
    public Page<Report> getByUsername(String username, Pageable pageable) {
        return reportRepository.findByUsername(username, pageable);
    }

    @Override
    public Page<Report> searchByUsernameAndPriority(String username, String priority, Pageable pageable) {
//        ***********************************************************************************************
//        Another implementation if I didn't use query for retrieving my data would be
//        ***********************************************************************************************
//        Optional optUsername = reportRepository.findAll().stream()
//                .filter(report -> report.getEmployee().getUsername().equalsIgnoreCase(username))
//                .findFirst();
//
//        if(!optUsername.isPresent())
//            return null;
//
//        List<Report> reportList  = reportRepository.findAll().stream()
//                .filter(report -> report.getEmployee().getUsername().equalsIgnoreCase(username))
//                .filter(report -> report.getPriority().equalsIgnoreCase(priority))
//                .collect(Collectors.toList());
//        ***********************************************************************************************

        return reportRepository.findByUsernamePriorityPage(username, priority, pageable);
    }
}
