package com.exercise.demo.service;

import com.exercise.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public boolean checkUsername(String username) {
        Optional opt = employeeRepository.findAll().stream()
                .filter(employee1 -> employee1.getUsername().equalsIgnoreCase(username))
                .findFirst();
        return (opt.isPresent());
    }

}
