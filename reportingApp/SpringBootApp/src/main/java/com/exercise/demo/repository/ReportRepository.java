package com.exercise.demo.repository;

import com.exercise.demo.model.Report;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;



public interface ReportRepository extends JpaRepository<Report,Integer> {
    @Query("select r from Report r " +
            "where priority like %?1%")
    Page<Report> findByPriority(String priority, Pageable pageable);

    @Query("select r from Report r,Employee e " +
            "where e.username like %?1% " +
            "and r.priority like %?2% " +
            "and e.employeeId = r.employee")
    Page<Report> findByUsernamePriorityPage(String username, String priority, Pageable pageable);

    @Query("select r from Report r,Employee e " +
            "where e.username like %?1% "+
            "and e.employeeId = r.employee")
    Page<Report> findByUsername(String username, Pageable pageable);

    @Query("select r from Report r")
    Page<Report> findAllReports( Pageable pageable);

    @Transactional
    @Modifying
    @Query("delete from Report r where r.id= ?1")
    void deleteReport( int id);
}

