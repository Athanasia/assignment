package com.exercise.demo.service;

public interface EmployeeService {
    boolean checkUsername(String username);
}
