package com.exercise.demo.service;


public interface DeleteService {

    String deleteReports(String reportIds);

}
