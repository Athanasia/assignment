package com.exercise.demo.service;

import com.exercise.demo.repository.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DeleteServiceImpl implements DeleteService {
    public static final Logger log = LoggerFactory.getLogger(DeleteServiceImpl.class);

    @Autowired
    private ReportRepository reportRepository;

    @Override
    public String deleteReports(String reportIds) {
        String result="";
        String[] stringIds = reportIds.split(",");
        List<Integer> intIds = new ArrayList<>();
        for (String id : stringIds){
            try{
                intIds.add(Integer.valueOf(id));
            }catch (Exception e){
                log.info(e.getMessage());
                result="Error reading the report ids, please check your input!";
            }
        }
        for(int id : intIds){
            Optional opt = reportRepository.findAll().stream()
                        .filter(report -> report.getReportId()==id)
                        .findFirst();
            if(opt.isPresent()){
                reportRepository.deleteReport(id);
                result+="Report with id "+id+" was deleted successfully!\n";
            }
            else
                result+="Report with id "+id+" was not found.\n";
        }
        return result;
    }

}
