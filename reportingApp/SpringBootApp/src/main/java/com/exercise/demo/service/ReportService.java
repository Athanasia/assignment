package com.exercise.demo.service;

import com.exercise.demo.model.Report;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ReportService {

    void parseJsonFile(String filename);

    boolean checkPriority (String priority);


    Page<Report> getAllReports(Pageable pageable);
}
