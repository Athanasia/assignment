package com.exercise.demo.controller;

import com.exercise.demo.model.Report;
import com.exercise.demo.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "store/")
public class StorageController {

    @Autowired
    private StoreService storeService;

    @PostMapping(value = "/report/employee={employeeId}")
    public ResponseEntity<?> storeNewReport(@PathVariable Optional<Integer> employeeId, @RequestBody List<Report> newReports, @RequestHeader Optional<Integer> pageNumber, @RequestHeader Optional<Integer> pageSize){
        boolean validRequest = storeService.checkIfValidRequest(newReports);
        if(!validRequest)
            return new ResponseEntity<String>("Invalid priorities! Acceptable values: low, high ", HttpStatus.BAD_REQUEST);
        Page<Report> results = storeService.createNewReport(newReports,employeeId,pageNumber,pageSize);
        if(null!=results)
            return new ResponseEntity<Page<Report>>(results, HttpStatus.OK);
        else if(null==results)
            return new ResponseEntity<String>("There's no such user! Please check the id again!",HttpStatus.NOT_ACCEPTABLE);
        else
            return new ResponseEntity<String>("Bad request! Please check your input!", HttpStatus.BAD_REQUEST);
    }
}
