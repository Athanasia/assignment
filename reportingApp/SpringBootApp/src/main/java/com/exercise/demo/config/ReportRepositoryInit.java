package com.exercise.demo.config;

import com.exercise.demo.service.ReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ReportRepositoryInit implements CommandLineRunner {
    @Autowired
    private ReportService reportService;

    @Override
    public void run(String... args) throws Exception {
        reportService.parseJsonFile("MOCK_DATA.json");
    }
}
