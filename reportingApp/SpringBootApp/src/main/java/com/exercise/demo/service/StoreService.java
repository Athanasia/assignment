package com.exercise.demo.service;

import com.exercise.demo.model.Report;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface StoreService {

    Page<Report> createNewReport(List<Report> newReports, Optional<Integer> employeeId, Optional<Integer> pageNumber, Optional<Integer> pageSize);

    boolean checkIfValidRequest(List<Report> newReports);
}
