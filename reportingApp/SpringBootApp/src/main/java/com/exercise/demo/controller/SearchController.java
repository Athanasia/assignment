package com.exercise.demo.controller;

import com.exercise.demo.model.Report;
import com.exercise.demo.service.EmployeeService;
import com.exercise.demo.service.ReportService;
import com.exercise.demo.service.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/search")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private EmployeeService employeeService;

    public static final Logger log = LoggerFactory.getLogger(SearchController.class);

    @GetMapping(value = "/reports/")
    public ResponseEntity<?> getAllReports(@RequestHeader("pageNumber") Optional<Integer> pageNumber, @RequestHeader("pageSize") Optional<Integer> pageSize){
        return new ResponseEntity<Page<Report>>(reportService.getAllReports(PageRequest.of(pageNumber.orElse(0), pageSize.orElse(10))), HttpStatus.OK);
    }

    @GetMapping(value = "/reports/priority={priority}")
    public ResponseEntity<?> getByPriority(@PathVariable Optional<String> priority,@RequestHeader("pageNumber") Optional<Integer> pageNumber, @RequestHeader("pageSize") Optional<Integer> pageSize){
        if(reportService.checkPriority(priority.orElse(null))==false)
            return new ResponseEntity<String>("Invalid priority type. Acceptable values: high, low . Please try again. ",HttpStatus.BAD_REQUEST);
        log.info("This is the priority: "+priority+" , pageNumber: "+pageNumber+" , pageSize: "+pageSize);
        Page<Report> reportPage = searchService.getByPriority(priority.orElse(" "), PageRequest.of(pageNumber.orElse(0), pageSize.orElse(10)));
        return new ResponseEntity<Page<Report>>(reportPage, HttpStatus.OK);
    }

    @GetMapping(value = "/reports/username={username}")
    public ResponseEntity<?> getByUsername(@PathVariable Optional<String> username,@RequestHeader("pageNumber") Optional<Integer> pageNumber, @RequestHeader("pageSize") Optional<Integer> pageSize){
        if(employeeService.checkUsername(username.orElse(null))==false)
            return new ResponseEntity<String>("Invalid username. There is no employee with such username. ",HttpStatus.BAD_REQUEST);
        log.info("This is the username: "+username+" , pageNumber: "+pageNumber+" , pageSize: "+pageSize);
        Page<Report> reportPage = searchService.getByUsername(username.orElse(" "), PageRequest.of(pageNumber.orElse(0), pageSize.orElse(10)));
        return new ResponseEntity<Page<Report>>(reportPage, HttpStatus.OK);
    }

    @GetMapping(value = "/reports/username={username}&priority={priority}")
    public ResponseEntity<?> getReportsByUsernameAndPriority(@PathVariable("username") Optional<String> username, @PathVariable("priority") Optional<String> priority, @RequestHeader("pageNumber") Optional<Integer> pageNumber, @RequestHeader("pageSize") Optional<Integer> pageSize){
        if(reportService.checkPriority(priority.orElse(""))==false)
            return new ResponseEntity<String>("Invalid priority type. Acceptable values: high, low . Please try again. ",HttpStatus.BAD_REQUEST);
        log.info("This is the username: "+username+" , priority: "+priority+", pageNumber: "+pageNumber+" , pageSize: "+pageSize);
        Page<Report> reportPage = searchService.searchByUsernameAndPriority(username.orElse(""),priority.orElse(""),PageRequest.of(pageNumber.orElse(0),pageSize.orElse(10)));
        return new ResponseEntity<Page<Report>>(reportPage, HttpStatus.OK);
    }
}
