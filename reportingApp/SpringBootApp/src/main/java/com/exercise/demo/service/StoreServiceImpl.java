package com.exercise.demo.service;

import com.exercise.demo.model.Employee;
import com.exercise.demo.model.PriorityTypes;
import com.exercise.demo.model.Report;
import com.exercise.demo.repository.EmployeeRepository;
import com.exercise.demo.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreServiceImpl implements StoreService {

    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Page<Report> createNewReport(List<Report> newReports, Optional<Integer> employeeId, Optional<Integer> pageNumber, Optional<Integer> pageSize) {
        Employee employee = checkIfEmployeeExists(newReports, employeeId);
        if(null!=employee){
            for(Report rep : newReports){
                Report newRep = new Report();
                newRep.setTitle(rep.getTitle());
                newRep.setPriority(rep.getPriority());
                newRep.setDescription(rep.getDescription());
                newRep.setEmployee(employee);
                employee.getReports().add(newRep);
                reportRepository.save(newRep);
            }
            return reportRepository.findByUsername(employee.getUsername(), PageRequest.of(pageNumber.orElse(0),pageSize.orElse(10)));
        }
        return null;
    }

    @Override
    public boolean checkIfValidRequest(List<Report> newReports) {
        boolean isPriorityCorrect = true;
        for(Report report: newReports){
            for(PriorityTypes type: PriorityTypes.values()) {
                if (!type.name().equalsIgnoreCase(report.getPriority())) {
                    isPriorityCorrect = false;
                }
            }
        }
        return isPriorityCorrect;
    }

    public Employee checkIfEmployeeExists(List<Report> report, Optional<Integer> employeeId) {
        if (null != employeeId) {
            Optional optionalEmployee = employeeRepository.findAll().stream()
                    .filter(writer1 -> writer1.getEmployeeId() == (employeeId.orElse((Integer)null)))
                    .findFirst();
            if (optionalEmployee.isPresent()) {
                return (Employee)optionalEmployee.get();
            }
        }
        return null;
    }
}
