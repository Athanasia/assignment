//package com.exercise.demo;
//
//import com.exercise.demo.controller.SearchController;
//import com.exercise.demo.model.Employee;
//import com.exercise.demo.model.Report;
//import com.exercise.demo.service.SearchService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.skyscreamer.jsonassert.JSONAssert;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageImpl;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.RequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(value = SearchController.class)
//public class SearchControllerTest {
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private SearchService searchService;
//
//    public static final Logger log = LoggerFactory.getLogger(SearchControllerTest.class);
//
//    @Test
//    public void getReportsByUsernameAndPriority () throws Exception{
//        List<Report> reportList = new ArrayList<>();
//        Report re1 = new Report(4,"HelloTesting1","Ola kalaa1","low",new Employee(4,"AthanasiaTesting1","Athanasia","Karagianni","athanasia@gmail.com","female","Ploiarxos",null));
//        Report re2 = new Report(5,"HelloTesting2","Ola kalaa2","high",new Employee(5,"AthanasiaTesting2","Athanasia2","Karagianni2","athanasia@gmail.com2","female","Ploiarxos2",null));
//        Report re3 = new Report(6,"HelloTesting3","Ola kalaa3","high",new Employee(4,"AthanasiaTesting1","Athanasia","Karagianni","athanasia@gmail.com","female","Ploiarxos",null));
//
//        reportList.add(re1);
//        reportList.add(re2);
//        reportList.add(re3);
//        Page<Report> reportPage = new PageImpl<>(reportList);
//        Pageable pageable = PageRequest.of(Mockito.anyInt(), 10);
//        Mockito.when(
//                searchService.searchByUsernameAndPriority(Mockito.anyString(),Mockito.anyString(),pageable)).thenReturn(reportPage);
//
//        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
//                "/search/username=AthanasiaTesting1&priority=low?page=0").accept(
//                MediaType.APPLICATION_JSON);
//
//        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
//        log.info(result.getResponse().toString());
//        String finalResult = result.getResponse()
//                .getContentAsString();
////        String expected = "{reportId: 7,title:HelloTesting1, description:Ola kalaa,priority:low, employee: [{employeeId: 6, username:AthanasiaTesting1,firstName:Athanasia,lastName:Karagianni,email:athanasia@gmail.com,gender:female,title:Ploiarxos}]}";
//        String expected = "";
//        JSONAssert.assertEquals(expected, result.getResponse()
//                .getContentAsString(), false);
//    }
//
//}
